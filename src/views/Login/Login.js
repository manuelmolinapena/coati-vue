import UserService from '../../services/users.service'
import EventBus from '../../lib/eventBus'
import httpError from '../../helpers/httpError'
const service = new UserService()

import {mapActions, mapGetters} from 'vuex'

export default {
  name: 'login',
  components: {},
  props: [],
  data() {
    return {
      name: '',
      password: ''
    }
  },
  computed: {
    user: {
      get () {
          return this.getUser()
      },
      set (value) {
          this.setUser(value)
      }
  }
  },
  mounted() {

  },
  methods: {
    ...mapActions(['setUser']),
    ...mapGetters(['getUser']),

    async Login(event) {
      if (event.target.checkValidity()) {
        const res = await service.Login({
          name: this.name,
          password: this.password
        })
        if (res && !res.error) {
          this.user = res
          this.$router.push({ name: 'dashboard' })
        } else {
          EventBus.$emit('alert', {
            success: false,
            message: httpError.parseError(res.error)
          })
        }
      }
    }
  }
}
