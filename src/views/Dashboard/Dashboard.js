import UserService from '../../services/users.service';
import EventBus from '../../lib/eventBus';
import UserModal from '../../components/NewUserModal';
import ConfirmModal from '../../components/ConfirmModal';
import { mapGetters } from 'vuex';

const services = new UserService();

export default {
  name: 'dashboard',
  components: {
    UserModal,
    ConfirmModal
  },
  props: [],
  data() {
    return {
      users: []
    }
  },
  computed: {
    ...mapGetters(['getUser']),
    user() {
      return this.getUser;
    },
    isAdmin() {
      return this.user.role === 'administrator';
    }
  },
  mounted() {
    this.Load();
    EventBus.$on('load-users', this.Load)
  },
  methods: {
    async Load() {
      const res = await services.List();
      if (res && !res.error) {
        this.users = res.data.users;
      }
    },
    Confirm(index) {
      if (typeof index === 'number') {
        const user = this.users[index]
        EventBus.$emit('confirmModalShow', user.id);
      }
    },
    showModal(index) {
      if (typeof index === 'number') {
        const user = this.users[index]

        this.user = {
          id: user._id,
          name: user.name,
          role: user.role
        }
        EventBus.$emit('createUserModalShow', user)
      } else {
        EventBus.$emit('createUserModalShow')
      }
    },
  }
}
