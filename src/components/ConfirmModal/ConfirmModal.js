import EventBus from '../../lib/eventBus';
import UserService from '../../services/users.service';

const userService = new UserService()

export default {
  name: 'confirm-modal',
  components: {},
  props: [],
  data: function () {
    return {
      showModalConf: false,
      id: ''
    }
  },
  computed: {

  },
  mounted() {
    EventBus.$on('confirmModalShow', (id) => {
      this.id = id;
      this.showModalConfirm();
    })
  },
  methods: {
    showModalConfirm() {
      this.showModalConf = true;
    },

    HideModal() {
      this.showModalConf = false
    },

    async Remove() {
      const res = await userService.Delete(this.id);
      if (res && !res.error) {
        EventBus.$emit('alert', { success: !res.error, message: res.message })
      } else {
        EventBus.$emit('alert', { success: res.error, message: `${res.message}: ${res.field}` })
      }
      EventBus.$emit('load-users');
      this.HideModal()
    }
  }
}

