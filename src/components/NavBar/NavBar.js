import {mapActions} from 'vuex';

export default {
  name: 'nav-bar',
  components: {},
  props: [],
  data () {
    return {

    }
  },
  computed: {

  },
  mounted () {

  },
  methods: {
    ...mapActions(['logout']),
    async Logout(){
      this.logout();
      this.$router.push({ name: 'login' });
    }
  }
}
