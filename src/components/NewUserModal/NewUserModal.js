import EventBus from '../../lib/eventBus';
import UserService from '../../services/users.service';

const userService = new UserService()

export default {
    name: 'new-user-modal',
    components: {},
    props: [],
    data: function() {
        return {
            showModal: false,
            user: this.newUser(),
            userType: [
                { type: 'user', text: 'User' },
                { type: 'administrator', text: 'Adminitrator' }
            ]
        }
    },
    computed: {

    },
    mounted () {
        EventBus.$on('createUserModalShow', (user) => {
            if (user === undefined) {
                this.showModalNew()
            } else {
                this.user.isNew = false;
                this.user.id = user.id;
                this.user.name = user.name;
                this.user.role = user.role;
                this.showModalNew()
            }
        })
    },
    methods: {
        showModalNew() {
            this.showModal = true;
        },
        newUser () {
            return {
                isNew: true,
                username: '',
                password: '',
                repeatPassword: '',
                role: 'user'
            }
        },

        async Create (event) {
            if (event.target.checkValidity()) {
                if (this.user.name !== '' && this.user.password !== '' && this.user.repeatPassword !== '' && this.user.role !== '') {
                    if (this.user.password === this.user.repeatPassword) {
                        let res;
                        if (!this.user.isNew) res = await userService.Update(this.user)
                        else res = await userService.Create(this.user);
                        if (res && !res.error) {
                            EventBus.$emit('alert', { success: !res.error, message: res.message })
                        } else {
                            EventBus.$emit('alert', { success: res.error, message: res.message });
                            return
                        }
                        EventBus.$emit('load-users')
                        this.HideModal()
                    } else {
                        EventBus.$emit('alert', {
                            success: false,
                            message: 'Password doesnt match'
                        })
                        return
                    }
                } else {
                    EventBus.$emit('alert', {
                        success: false,
                        message: 'Information incomplete'
                    })
                }
            }
        },

        async HideModal () {
            this.reset()
            this.showModal = false
        },

        reset () {
            this.user = this.newUser()
        }

    }
}
