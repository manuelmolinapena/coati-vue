/* eslint-disable */

class httpError {
    /**
     * 
     * @param {object} error
     */
    static parseError(error) {
        let message = '';

        if(error.message.includes('400')) message = 'Bad request';
        else if(error.message.includes('403')) message = 'Invalid credentials';
        else if(error.message.includes('500')) message = 'Iternal server error';
        else message = '';

        return message;
    }
}

export default httpError;