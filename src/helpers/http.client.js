import axios from 'axios';

class HttpClient {
    constructor(options = {}) {
        this.instance = axios.create(options)
    }

    async get(apiPath, headers = {}) {
        headers['Authorization'] = 'Bearer ' + localStorage.getItem('token');
        headers['key'] = localStorage.getItem('hash');
        return this.instance.get(apiPath, { headers: headers });
    }

    async post(apiPath, params = {}, headers = {}) {
        headers['Authorization'] = 'Bearer ' + localStorage.getItem('token');
        headers['key'] = localStorage.getItem('hash');
        return this.instance.post(apiPath, params, { headers: headers });
    }

    async put(apiPath, params = {}, headers = {}) {
        headers['Authorization'] = 'Bearer ' + localStorage.getItem('token');
        headers['key'] = localStorage.getItem('hash');
        return this.instance.put(apiPath, params, { headers: headers });
    }

    async delete(apiPath, headers = {}) {
        headers['Authorization'] = 'Bearer ' + localStorage.getItem('token');
        headers['key'] = localStorage.getItem('hash');
        return this.instance.delete(apiPath, { headers: headers });
    }
}

export default HttpClient;