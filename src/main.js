import Vue from 'vue'
import App from './App.vue'
import router from './router'
import EventBus from './lib/eventBus.js'
import axios from 'axios'
import store from './store/store'
import vselect from 'vue-select'
import 'bootstrap/dist/css/bootstrap.css'
import VueSpinners from 'vue-spinners'


Vue.prototype.$bus = EventBus
Vue.prototype.$http = axios

Vue.config.productionTip = false

Vue.component('vselect', vselect)
Vue.use(VueSpinners)


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
