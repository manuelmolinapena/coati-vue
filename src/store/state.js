export default {
    isLoggedIn: !!localStorage.getItem('token'),
    user: JSON.parse(localStorage.getItem('user')),
    token: `Barier: ${localStorage.getItem('token')}`
}