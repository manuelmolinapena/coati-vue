
export default {
    isLoggedIn: state => state.isLoggedIn,
    getUser: state => state.user,
    isAdmin: state => state.admin,
    token: state => state.token,
}
