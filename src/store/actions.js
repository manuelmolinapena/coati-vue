import { mutationsTypes } from './mutations'

export default {
    load ({ commit }, status) {
        commit(mutationsTypes.LOAD, status)
    },

    setUser ({ commit }, user) {
        commit(mutationsTypes.LOGIN_SUCCESS, user)
    },

    logout ({ commit }) {
        commit(mutationsTypes.LOGOUT)
    }
}
