export const mutationsTypes = {
    LOGIN_SUCCESS: 'LOGIN_SUCCESS',
    LOGOUT: 'LOGOUT'
}

export const mutations = {
    [mutationsTypes.LOGIN_SUCCESS] (state, user) {
        const expires = new Date()
        expires.setHours(expires.getHours() + 24)
        expires.setMinutes(expires.getMinutes() - 1)

        localStorage.setItem('expires', expires.getTime().toString())
        localStorage.setItem('token', user.token)
        localStorage.setItem('user', JSON.stringify(user.user))
        state.user = user.user
        state.token = user.token
        state.isLoggedIn = true
        state.admin = user.user.role
    },
    [mutationsTypes.LOGOUT] (state) {
        localStorage.removeItem('token')
        localStorage.removeItem('user')
        localStorage.removeItem('expires')
        state.user = {}
        state.isLoggedIn = false
        state.token = ''
        state.admin = false
    }
}
