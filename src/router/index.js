/* eslint-disable */

import Vue from 'vue';
import Router from 'vue-router';
import Layout from '../views/Layout';
import Login from '../views/Login';
import Dashboard from '../views/Dashboard'

Vue.use(Router)


const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Layout,
      children: [
        {
          path: 'dashboard',
          name: 'dashboard',
          component: Dashboard,
        },
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
  ],
  linkActiveClass: 'active',
});

router.beforeEach((to, from, next) => {
  const expire = new Date(Number(localStorage.getItem('expires')));
  const now = new Date();

  if (localStorage.getItem('token') && localStorage.getItem('user') && expire > now) {
    const permissions = JSON.parse(localStorage.getItem('user')).role;

    if (to.name === 'home') return next({ name: 'dashboard' });
    if (to.name === 'login') return next({ name: 'dashboard' });
    if (to.meta && (to.meta.isAdmin && !permissions.isAdmin) && (to.meta.isDealer && !permissions.isDealer)) return next({ name: 'dashboard' });
    return next();
  } else if (to.name === 'login') return next();
  else return next({ name: 'login' });
});

export default router;