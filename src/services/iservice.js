/* eslint-disable */

import HttpClient from "../helpers/http.client";
import config from "../config";

class IService {
    constructor(http) {
        this.baseURL = config[config.production ? "prod" : "dev"].api_server;
        this.http = http;
        if (!http) this.http = new HttpClient({ baseURL: this.baseURL });
    }
}

export default IService;
