import IService from './iservice'

class UserService extends IService {
    constructor () {
        super()
        this.path = '/user'
    }

    async Login (data) {
        try {
            const res = await this.http.post(`${this.path}/login`, data)
            return res.data
        } catch (err) {
            return {
                error: err
            }
        } 
    }

    async List () {
        try {
            const res = await this.http.get(`${this.path}/list`);
            return res.data
        } catch (err) {
            console.log(err);
            return {
                error: err
            }
        }
    }

    async Create (data) {
        try {
            const res = await this.http.post(`${this.path}/create`, data)
            return res.data
        } catch (err) {
            return null
        }
    }

    async Update (data) {
        try {
            const res = await this.http.put(`${this.path}/update`, data)
            return res.data
        } catch (err) {
            return null
        }
    }

    async Delete (id) {
        try {
            const res = await this.http.delete(`${this.path}/delete/${id}`)
            return res.data
        } catch (err) {
            return null
        }
    }
}

export default UserService
